## Q-SYS Zoom Room Controls

This example script creates a script that interfaces Zoom Room Controls into Q-SYS. 

An example .qsys file and ZRC JSON config is included in the repositority.

Setup Instructions:  

1. Add a text controller to your design  
2. Add the following controls to the text controller:  
	- Status (Status)  
	- Controls as needed for your integration (see example image below) 
3. Add a Proxy Monitor to your design and name it ZoomRoom 
4. Wire the output of the status control into the Proxy Monitor.


Sample Integration:  
![picture](Screenshot.PNG)  


License: MIT